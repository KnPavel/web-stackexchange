Test project version 0.0.1
==========================

Installation
------------

To have our WAR file deployed and running in Tomcat, we need to complete the following steps:
1. [Download Apache Tomcat](http://mirror.23media.de/apache/tomcat/tomcat-8/v8.5.30/bin/) and install it to the server.
2. In the project go to the root folder and run the command: 
    
    mvn clean install

3. Copy our WAR file from _build_ folder to the _tomcat/webapps_ folder
4. Run the tomcat server
5. Our application will be available by link _http://localhost:8888_