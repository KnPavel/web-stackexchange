package com.piano.web.restful;

import com.piano.web.dto.stackexchange.StackexchangeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

/**
 * Created by pavel on 17.03.19.
 */
@Component
public class SearchClientRequestService {

    private RestTemplate restTemplate;

    @Value("${stackexchange.api}")
    private String url;

    @Autowired
    public SearchClientRequestService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public StackexchangeResponse getSearchResultByString(String request) {
        if (StringUtils.isEmpty(request)) {
            return new StackexchangeResponse();
        }
        return restTemplate.getForObject(url, StackexchangeResponse.class, request);
    }
}
