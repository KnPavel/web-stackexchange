package com.piano.web.controller;

import com.piano.web.dto.stackexchange.Items;
import com.piano.web.dto.stackexchange.StackexchangeResponse;
import com.piano.web.restful.SearchClientRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by pavel on 17.03.19.
 */
@Controller
@RequestMapping("/")
public class SearchController {

    private SearchClientRequestService clientRequestService;

    @Autowired
    public SearchController(SearchClientRequestService clientRequestService) {
        this.clientRequestService = clientRequestService;
    }

    @GetMapping
    public String startPage() {
        return "index";
    }

    @GetMapping("/search")
    public String search(@RequestParam("value") String request, Model view) {
        StackexchangeResponse response = clientRequestService.getSearchResultByString(request);
        Items[] items = response.getItems();
        if (itemsIsEmpty(items)) {
            return "fragment/snippets :: no-data";
        } else {
            view.addAttribute("items", response.getItems());
            return "fragment/snippets :: table-result";
        }
    }

    private boolean itemsIsEmpty(Items[] items) {
        return items == null || items.length == 0;
    }
}
