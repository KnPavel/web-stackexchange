package com.piano.web.dto.stackexchange;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by pavel on 17.03.19.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Items implements Serializable {
    private Owner owner;
    private String score;
    private String link;
    private Long lastActivityDate;
    private String isAnswered;
    private Long creationDate;
    private Integer answerCount;
    private String title;
    private String questionId;
    private String viewCount;
    private String[] tags;
}
