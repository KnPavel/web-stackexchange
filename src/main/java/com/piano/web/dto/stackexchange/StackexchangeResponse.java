package com.piano.web.dto.stackexchange;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by pavel on 17.03.19.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class StackexchangeResponse implements Serializable {

    private Long quotaMax;
    private Long quotaRemaining;
    private Boolean hasMore;
    private Items[] items;
}
