$(document).ready(function() {

    $('#search-input').keyup(function(e) {
        //code enter
        if(e.keyCode == 13) {
            getStackexchangeData();
        }
    });
});

function getStackexchangeData() {
    var url = '/search?value=';

    if ($('#search-input').val() !== '') {
        url = url + encodeURIComponent($('#search-input').val());
    }

    $("#result-data").load(url);
}
