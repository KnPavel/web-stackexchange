package com.piano.web.restful;

import com.piano.web.dto.stackexchange.StackexchangeResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

/**
 * Created by pavel on 26.03.19.
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchClientRequestServiceTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    SearchClientRequestService clientRequestService;

    @Value("${stackexchange.api}")
    private String url;

    private static String EMPTY = "";

    @Test
    public void checkReturnEmptyResult_WhenEmptyRequest() {
        StackexchangeResponse searchResultByString = clientRequestService.getSearchResultByString(EMPTY);
        StackexchangeResponse expectedEmptyObject = new StackexchangeResponse();
        assertEquals(expectedEmptyObject, searchResultByString);
    }

    @Test
    public void checkSpecificStackexchangeResult() {
        Mockito.when(restTemplate.getForObject(url, StackexchangeResponse.class, "test"))
                .thenReturn(new StackexchangeResponse(2L, 1L, false, null));

        StackexchangeResponse expectedObject = new StackexchangeResponse(2L, 1L, false, null);
        StackexchangeResponse result = clientRequestService.getSearchResultByString("test");
        assertEquals(expectedObject, result);
    }
}
